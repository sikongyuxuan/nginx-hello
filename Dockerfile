FROM nginxdemos/hello

LABEL maintainer="Rinat Kirimov"
LABEL description="Demo NGINX container for DevOps Assessment"

WORKDIR /usr/share/nginx/html
COPY index.html index.html
