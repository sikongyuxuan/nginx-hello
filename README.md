# Introduction

This tutorial will guide you through the steps to create Docker image from `Dockerfile` based on [NGINX](https://hub.docker.com/r/nginxdemos/hello) demo image.

# Getting started

## Building the Docker image<a name="step-1"></a>

Copy the repo to your local environment using git:

```bash
git clone https://gitlab.com/highwatersdev/nginx-hello.git
cd nginx-hello
```

In the current directory, run the following command to build Docker image from Dockerfile:
```bash
docker build --tag localhost:nginx-hello .
```

## Running the container

Start the container from the newly built image:

```bash
docker run --name nginx-hello -d \
  localhost:nginx-hello
```

## Accessing the webpage hosted on NGINX container

By default, the container runs on port 80 and can be accessed via container's IP address:

```bash
docker inspect -f \
  '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
  nginx-hello
```

```bash
curl http://<container IP_Address>
```

## Removing the container

You can remove the running container via the following command:

```bash
docker stop nginx-hello && docker rm nginx-hello
```

## Removing the Docker image

You can also remove the nginx-hello image via the following command:

> **Note**: you need to remove the container first before removing the image

```bash
docker rmi localhost:nginx-hello
```

## Configuration

#### Replacing index.html file
If you would like to run your own HTML page inside NGINX container:
- replace the content of `index.html` file under `nginx-hello` directory with your desired content.
- rebuild the Docker image as we did [here](#step-1)
  ```bash
  docker build --tag localhost:nginx-custom .
  ``` 

#### Accessing container under a different port
To access the container under a different port, for example port **8081**, you'll need to expose the container at the run time:

```bash
docker run --name nginx-hello -d -p 8081:80 \
  localhost:nginx-hello
```
Access the container via port **8081**:
```bash
curl http://<container IP_Address>:8081
```
>**Note**: if you get "_connection refused_" error, try accessing it via localhost
>```bash
>curl http://localhost:8081
>```
